console.log('Loading function');
var AWS = require('aws-sdk');
var dynamo = new AWS.DynamoDB.DocumentClient();
var table = "Sdcc1617SignalationsTableDeploy";

exports.handler = function(event, context) {
    //console.log('Received event:', JSON.stringify(event, null, 2));
   var params = {
    TableName:table,
    Item:{
  	  "created": event.created,
      "image":  event.image,
      "latitude": event.latitude,
      "longitude": event.longitude,
      "message": event.message,
      "stateId": event.stateId,
      "title": event.title,
      "topic": event.topic,
      "created":event.created,
      "by": event.by
	  }
	
        
    };

    console.log("Adding a new IoT device...");
    dynamo.put(params, function(err, data) {
        if (err) {
            console.error("Unable to add device. Error JSON:", JSON.stringify(err, null, 2));
            context.fail();
        } else {
            console.log("Added device:", JSON.stringify(data, null, 2));
            context.succeed();
        }
    });
}
