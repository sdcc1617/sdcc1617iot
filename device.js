var awsIot = require('aws-iot-device-sdk');

'use strict';
const args = require('yargs').argv;
//'./certs/d1521cc16d-private.pem.key'
var keyPath = args.keyPath
//'./certs/d1521cc16d-certificate.pem.crt'
var certPath = args.certPath
//'./certs/rootCA.pem'
var caPath = args.caPath
//'raspberrypi'
var clientId = args.clientId
var deviceId = args.name
//'a1yjgaiunqyy3l.iot.eu-west-1.amazonaws.com'
var host = args.host

//I topic a cui ci si sottoscrive
var registrationTopic = 'registration'
var iotSignalationTopic = 'iotSignalationTopic'

//verificare se sia possibile prenderle da Google Maps
var gpsLongitude = "41.7247656"
var gpsLatitude =  "13.1370926"

var etcdAddress = "127.0.0.1"
//"sdcc1617utilityapp"
var bucket = args.bucketName
//d1egz9u6mhouz8.cloudfront.net/"
var cloudFront = "http://"+args.cdnName

//nome del client, è lo stesso su aws
//var clientId = "raspberrypi"
//email, ad esempio di un responsabile del sensore
var email = "falberto@hotmail.it"
//endpoint, lo stesso su aws
var endpoint = args.host
// tipo del sensore
var type = "MySmartIoTDevice"

var num=16;
function makeid(num) {
  var text = "";
  var possible = "1234567890";

  for (var i = 0; i < num; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}
function makestring(num) {
    var text = "";
    var possible = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    for (var i = 0; i < num; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

function makealfastring(num) {
    var text = "";
    var possible = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    for (var i = 0; i < num; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

function makestringnumber(num) {
    var text = "";
    var possible = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    for (var i = 0; i < num; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

/**
 * Funzione che dovrebbe fare uso di visione artificiale, pattern matching, machine learning, e/o reinforcement learning.. 
 * Non implementata perché asula dal progetto
 */
function isCondition(){
   if(Math.random()<1)
      return true;
    else 
      return false;
}


/**
* Parametri di configurazione del device, ad esempio letti da file esterno. Servono per la registrazione del device,
* e per tenere traccia di qual è il device in questione. Ai fini del progetto sono inseriti staticamente.
*/

var reg = {
        
        "clientId": deviceId,//ID-91B2F06B3F05",
        "serialNumber": "SN-"+makealfastring(12),
        "activationCode": "AC-"+makealfastring(20),
        "activated": "true",
        "device": "Raspberry Pi",
        "type": type,
        "email": email,
        "endpoint": endpoint,
        "registrated": new Date().toString()
}

/**
 * Invocato la prima volta, per la registrazione
 */
var device = awsIot.device({
   keyPath: keyPath,
   certPath: certPath,
   caPath: caPath,
   clientId: clientId,
   host: host
});


device.publish('registration', 
  JSON.stringify(reg));

/** 
 * Job di monitoring avviato utilizzando il servizio di cron offerto dal sistema operativo Linux:
 * l'idea è che l'applicazione esegue un algoritmo periodicamente, acquisendo ad esempio un fotogramma dalla
 * telecamera di cui esso è attrezzato. Nel caso in cui questo si verificasse, allora quel che viene fatto
 * è inviare attraverso il protocollo AMPQ un messaggio sul topic relativo alle segnalazioni. 
 */

var cron = require('cron');
var i = 0 
var jobController = new cron.CronJob({
  cronTime:'*/10 * * * * *',
  onTick: function() {
        i++;
        console.log(i)
        if(isCondition()){
            // Url che può essere preso ad esempio invocando il servizio che salva su s3 una eventuale immagine catturata
            var topic = "Rifiuti";
            var title = "Chaos! Roma sommersa di rifiuti";
            // Id che può essere recuperato creando lo stato su Etcd, ad esempio invocando il servizio di Go
            //var stateId = "/dir/sRvpHi9c6FRXvuwiTwXNbgCJKCto2ikNM8pdjTjzAL0wr1P9BfaAA1etC7glM6uIaX0byBd5lu6oL2weD45jVuukc9ceXHAxAMkD5hMEhM8E0EAUcDxaEzQwp5cWJjZz";
            var message = "La solita storia a Roma da 20 anni.. Il comune faccia il suo dovere!";
            var sub = makeid(num) 
          
            console.log('connect');
            var AWS = require('aws-sdk');
            fs = require('fs');

            // Read in the file, convert it to base64, store to S3
            var fileStream = fs.createReadStream('./images/rapina_in_corso1.jpg');
            fileStream.on('error', function (err) {
              if (err) { throw err; }
            });  
            var filename = makestring(128) 
            fileStream.on('open', function () {
              var s3 = new AWS.S3();
              s3.putObject({
                Bucket: bucket,
                Key: "images/"+filename+".jpg",
                ACL: 'public-read',
                Body: fileStream
              }, function (err) {
                if (err) { throw err; }
              });
            });
         
            var urlCdnImage = cloudFront +filename+".jpg"

            //parlare con ETCD
            
            var http = require('http');

            var headers = {
                'Content-Type': 'application/json'
            };
            var stateId = "/dir/" + makestringnumber(128);
            var value =  "TODO" + "|" +  gpsLongitude + sub + "|" + gpsLatitude + sub ;
            var options = {
                host: etcdAddress,
                path: '/v2/keys'+stateId+"?value="+value,
                port: 2379,
                method: 'PUT',
                headers: headers
            };


            var callback = function(response) {
                  var str = '';

                  //another chunk of data has been recieved, so append it to `str`
                  response.on('data', function(chunk) {
                  str += chunk;
                  });

                  //the whole response has been recieved, so we just print it out here
                  response.on('end', function() {
                  console.log(str);
                  });
              };

            http.request(options, callback).end();
            //http.request(options, callback).write();

            device.publish(iotSignalationTopic, 
            JSON.stringify(
              {
                  "created": new Date().toString(),
                  "image": urlCdnImage,
                  "longitude": gpsLongitude + sub,
                  "latitude": gpsLatitude + sub,
                  "message": message,
                  "stateId": stateId,
                  "title":  title,
                  "topic": topic,
                  "by": deviceId
                }
            ));
        }
  },
  start: false,
  timeZone: 'America/Los_Angeles'
});

console.log("Start sensor session!");

jobController.start();
